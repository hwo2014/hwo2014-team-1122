(ns user
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.pprint :refer (pprint)]
            [clojure.repl :refer :all]
            [clojure.test :as test]
            [clojure.tools.namespace.repl :refer (refresh refresh-all)]
            [hwo2014bot.system :as system]
            [hwo2014bot.message-log :as mlog]
            [hwo2014bot.model.track :as track]
            [hwo2014bot.dashboard.server :as server]
            [hwo2014bot.race-files :as race-files]
            [clojure.java.shell :refer [sh]]))

(defonce system nil)

(defn init
  "Constructs the current development system."
  ([]
     (alter-var-root #'system
                     (constantly (system/system))))
  ([host]
     (alter-var-root #'system
                     (constantly (system/system {:connection {:host host}})))))

(defn start
  "Starts the current development system."
  []
  (alter-var-root #'system system/start))

(defn stop
  "Shuts down and destroys the current development system."
  []
  (alter-var-root #'system
    (fn [s] (when s (system/stop s)))))

(defn go
  "Initializes the current development system and starts it running."
  ([]
     (init)
     (start))
  ([host]
     (init host)
     (start)))

(defn reset []
  (stop)
  (refresh))

(defn latest-race-file
  []
  (reduce (fn [result f] (if (< (.lastModified result) (.lastModified f)) result f))
          (file-seq (clojure.java.io/file "races"))))

(defn load-latest-race
  []
  (when-let [f (latest-race-file)]
    (system/load-race f)))

(defn gnuplot
  ([] (gnuplot "/tmp/data.txt"))
  ([f]
     (spit f
           (apply str
                  (map (fn [e]
                         (apply str (interpose " "
                                               [(get-in e [:data :new :game-tick])
                                                (get-in e [:data :new :speed])
                                                (get-in e [:data :new :acceleration])
                                                "\n"])))
                       (filter #(= :state (:type %))
                               @(:log (:message-logger system))))))
     (apply sh ["gnuplot"
                "-e" "set terminal svg size 1200 1000"
                "-e" (str "set output \"" f ".svg\"")
                "-e" (str "plot \"" f "\" using 1:2 with linespoints title 'speed', \"" f "\" using 1:3 with points title 'acceleration'")])))
