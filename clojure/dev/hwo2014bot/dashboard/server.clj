(ns hwo2014bot.dashboard.server
  (:require [chord.http-kit :refer [with-channel]]
            [org.httpkit.server :refer [run-server]]
            [clojure.core.async :refer [go go-loop >! <! close!]]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [hwo2014bot.message-log :as mlog]
            [hwo2014bot.system :as system]
            [hwo2014bot.race-files :as race-files]))

(defn race-header-for-file
  [f]
  (let [race (race-files/load-race f)
        log (:log race)]
    {:file f
     :last-modified (.lastModified f)
     :game-init-message (mlog/find-game-init-message log)
     :game-end-message (mlog/find-game-end-message log)}))

(def memoized-race-header-for-file (memoize race-header-for-file))

(defn race-headers
  []
  (map memoized-race-header-for-file (race-files/list-files)))

(defn send-race-headers
  [ws-ch]
  (go
    (>! ws-ch
        {:type :race-headers
         :data (let [r (map #(dissoc % :file) (race-headers))]
                 (doall r))})))

(defn find-race-by-id
  [id]
  (when-let [race-header (->> (race-files/list-files)
                              (map race-header-for-file)
                              (filter #(= id (get-in % [:game-init-message :gameId])))
                              (first))]
    (race-files/load-race (:file race-header))))

(defn send-race
  [ws-ch race-id]
  (go
    (>! ws-ch
        (if-let [race (find-race-by-id race-id)]
          {:type :race-log
           :data race}
          {:type :error
           :data {:message (str "Race by id " race-id " not found.")}}))))

(defn start-race
  [races ws-ch]
  (if (contains? @races ws-ch)
    (>! ws-ch {:type :error :message "Race already started."})
    (swap! races assoc ws-ch
           (-> (system/system {:handle-msg-wrapper (fn [handle-msg]
                                                     (fn [msg state]
                                                       (let [response (handle-msg msg state)]
                                                         (go
                                                           (>! ws-ch {:type :live-game-update
                                                                      :data {:msg msg
                                                                             :response response}}))
                                                         response)))
                               :on-complete (fn []
                                              (when-let [system (get @races ws-ch)]
                                                (system/save-race system)
                                                (swap! races dissoc ws-ch)))})
               (system/start)))))

(defn handler [connections races req]
  (println "Starting handler...")
  (with-channel req ws-ch
    (println "Connected...")
    (swap! connections conj {:web-socket ws-ch})
    (go-loop []
      (when-let [message (:message (<! ws-ch))]
        (println "Got message:" message)
        (case (:type message)
          :ping (do
                  (>! ws-ch {:type :pong})
                  (recur))
          :request-race-headers (do
                                  (send-race-headers ws-ch)
                                  (recur))
          :load-race (do
                       (println message)
                       (send-race ws-ch (get-in message [:data :race-id]))
                       (recur))
          :start-race (do
                        (start-race races ws-ch)
                        (recur))
          (recur)))
      (swap! connections (fn [connections] (apply disj connections (filter #(= ws-ch (:web-socket %)) connections))))
      (println "Connection closed."))))

(defn system
  []
  {:connections (atom #{})
   :races (atom {})})

(defn start
  [system]
  (assoc system :server (run-server (partial handler (:connections system) (:races system)) {:port 3000})))

(defn stop
  [system]
  (update-in system [:server] (fn [stop-server]
                                (stop-server)
                                nil)))

