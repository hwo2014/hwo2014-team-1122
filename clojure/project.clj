(defproject hwo2014bot "0.1.0-SNAPSHOT"
  :description "HWO2014 Clojure Bot Template"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [aleph "0.3.2"]
                 [org.clojure/data.json "0.2.3"]
                 [org.clojure/tools.logging "0.2.6"]
                 [log4j "1.2.17"]
                 ;[jarohen/chord "0.3.1"]
                 ]
  :main ^:skip-aot hwo2014bot.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:repl-options {:init-ns user}
                   :source-paths ["dev"]
                   :dependencies [[midje "1.6.3"]
                                  [org.clojure/tools.namespace "0.2.3"]
                                  [org.clojure/java.classpath "0.2.0"]]}})
