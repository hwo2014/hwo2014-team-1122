(ns hwo2014bot.model-track-test
  (:require [midje.sweet :refer :all]
            [hwo2014bot.model.track :as track]
            [hwo2014bot.model.route :as route]))

(def keimola {:id "keimola",
              :name "Keimola",
              :pieces
              [{:length 100.0}
               {:length 100.0}
               {:length 100.0}
               {:length 100.0, :switch true}
               {:radius 100, :angle 45.0}
               {:radius 100, :angle 45.0}
               {:radius 100, :angle 45.0}
               {:radius 100, :angle 45.0}
               {:radius 200, :angle 22.5, :switch true}
               {:length 100.0}
               {:length 100.0}
               {:radius 200, :angle -22.5}
               {:length 100.0}
               {:length 100.0, :switch true}
               {:radius 100, :angle -45.0}
               {:radius 100, :angle -45.0}
               {:radius 100, :angle -45.0}
               {:radius 100, :angle -45.0}
               {:length 100.0, :switch true}
               {:radius 100, :angle 45.0}
               {:radius 100, :angle 45.0}
               {:radius 100, :angle 45.0}
               {:radius 100, :angle 45.0}
               {:radius 200, :angle 22.5}
               {:radius 200, :angle -22.5}
               {:length 100.0, :switch true}
               {:radius 100, :angle 45.0}
               {:radius 100, :angle 45.0}
               {:length 62.0}
               {:radius 100, :angle -45.0, :switch true}
               {:radius 100, :angle -45.0}
               {:radius 100, :angle 45.0}
               {:radius 100, :angle 45.0}
               {:radius 100, :angle 45.0}
               {:radius 100, :angle 45.0}
               {:length 100.0, :switch true}
               {:length 100.0}
               {:length 100.0}
               {:length 100.0}
               {:length 90.0}],
              :lanes
              [{:distanceFromCenter -10, :index 0}
               {:distanceFromCenter 10, :index 1}],
              :startingPoint {:position {:x -300.0, :y -44.0}, :angle 90.0}})

(fact (count (:pieces keimola)) => 40)

(fact (track/bend-switch-length (get-in keimola [:pieces 8]) -10 10) => (roughly 76.7))
(fact (track/bend-switch-length (get-in keimola [:pieces 8]) 10 -10) => (roughly 80.6))
(fact (track/bend-length (get-in keimola [:pieces 8]) 10) => (roughly 74.6))
(fact (track/bend-length (get-in keimola [:pieces 8]) -10) => (roughly 82.5))

(fact "lane combinations"
  (fact "keimola"
    (let [[first-lane second-lane] (:lanes keimola)]
      (track/lane-combinations keimola) => (just [{:start-lane first-lane :end-lane first-lane}
                                                  {:start-lane second-lane :end-lane second-lane}
                                                  {:start-lane first-lane :end-lane second-lane}
                                                  {:start-lane second-lane :end-lane first-lane}]
                                                 :in-any-order))))

(fact "route segments"
  (fact "keimola"
    (track/route-segments keimola)
    =>
    (just [[{:index 0, :piece {:length 100.0}}
            {:index 1, :piece {:length 100.0}}
            {:index 2, :piece {:length 100.0}}]
           [{:index 3, :piece {:switch true, :length 100.0}}]
           [{:index 4, :piece {:radius 100, :angle 45.0}}
            {:index 5, :piece {:radius 100, :angle 45.0}}
            {:index 6, :piece {:radius 100, :angle 45.0}}
            {:index 7, :piece {:radius 100, :angle 45.0}}]
           [{:index 8, :piece {:radius 200, :switch true, :angle 22.5}}]
           [{:index 9, :piece {:length 100.0}}
            {:index 10, :piece {:length 100.0}}
            {:index 11, :piece {:radius 200, :angle -22.5}}
            {:index 12, :piece {:length 100.0}}]
           [{:index 13, :piece {:switch true, :length 100.0}}]
           [{:index 14, :piece {:radius 100, :angle -45.0}}
            {:index 15, :piece {:radius 100, :angle -45.0}}
            {:index 16, :piece {:radius 100, :angle -45.0}}
            {:index 17, :piece {:radius 100, :angle -45.0}}]
           [{:index 18, :piece {:switch true, :length 100.0}}]
           [{:index 19, :piece {:radius 100, :angle 45.0}}
            {:index 20, :piece {:radius 100, :angle 45.0}}
            {:index 21, :piece {:radius 100, :angle 45.0}}
            {:index 22, :piece {:radius 100, :angle 45.0}}
            {:index 23, :piece {:radius 200, :angle 22.5}}
            {:index 24, :piece {:radius 200, :angle -22.5}}]
           [{:index 25, :piece {:switch true, :length 100.0}}]
           [{:index 26, :piece {:radius 100, :angle 45.0}}
            {:index 27, :piece {:radius 100, :angle 45.0}}
            {:index 28, :piece {:length 62.0}}]
           [{:index 29, :piece {:radius 100, :switch true, :angle -45.0}}]
           [{:index 30, :piece {:radius 100, :angle -45.0}}
            {:index 31, :piece {:radius 100, :angle 45.0}}
            {:index 32, :piece {:radius 100, :angle 45.0}}
            {:index 33, :piece {:radius 100, :angle 45.0}}
            {:index 34, :piece {:radius 100, :angle 45.0}}]
           [{:index 35, :piece {:switch true, :length 100.0}}]
           [{:index 36, :piece {:length 100.0}}
            {:index 37, :piece {:length 100.0}}
            {:index 38, :piece {:length 100.0}}
            {:index 39, :piece {:length 90.0}}]])))

(fact "route segment lane lenghts"
  (fact "keimola"
    (let [entries (track/route-segment-lane-length-entries (track/route-segments keimola) (:lanes keimola))
          direct-first-lane {:start-lane {:index 0, :distanceFromCenter -10}, :end-lane {:index 0, :distanceFromCenter -10}}
          direct-second-lane {:start-lane {:index 1, :distanceFromCenter 10}, :end-lane {:index 1, :distanceFromCenter 10}}
          first-to-second-lane {:start-lane {:index 0, :distanceFromCenter -10}, :end-lane {:index 1, :distanceFromCenter 10}}
          second-to-first-lane {:start-lane {:index 1, :distanceFromCenter 10}, :end-lane {:index 0, :distanceFromCenter -10}}]

      (fact "all entries have :route-segment and :lane-lengths keys"
        (every? (comp = #{:route-segment :lane-lengths}) (map (comp set keys) entries)) => true)

      (fact "number of entries"
        (count entries) => 15)
      
      (fact "first entry"
        (let [{:keys [route-segment lane-lengths]} (first entries)]
          lane-lengths => (just {direct-first-lane 300.0,
                                 direct-second-lane 300.0})))
      (fact "second entry"
        (let [{:keys [route-segment lane-lengths]} (second entries)]
          route-segment => (checker [x] (track/switch-segment? x))
          lane-lengths => (just {direct-first-lane 100.0,
                                 direct-second-lane 100.0,
                                 first-to-second-lane (roughly 101.98)
                                 second-to-first-lane (roughly 101.98)})))

      (fact "last entry"
        (let [{:keys [route-segment lane-lengths]} (last entries)]
          lane-lengths => (just {direct-first-lane 390.0,
                                 direct-second-lane 390.0})))

      (fact "shortest route"
        (let [result (route/shortest-route entries 0 1 3)]
          (:total-length result) => (roughly 9934)
          (count (:route result)) => 45
          (count (route/remaining-route (:route result) 2 0)) => 30
          (route/target-lane-index (drop 3 (:route result))) => 0)))))
