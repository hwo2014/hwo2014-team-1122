(ns hwo2014bot.message-log)

(defn sent-messages
  [log]
  (->> log
       (filter #(= :sent-message (:type %)))
       (map :message)))

(defn received-messages
  [log]
  (->> log
       (filter #(= :received-message (:type %)))
       (map :message)))

(defn messages-with-game-id
  [log]
  (->> log
       (received-messages)
       (filter :gameId)))

(defn find-game-id
  [log]
  (when-first [message (messages-with-game-id log)]
    (:gameId message)))

(defn find-game-init-message
  [log]
  (->> log
       (received-messages)
       (filter #(= "gameInit" (:msgType %)))
       (first)))

(defn find-game-end-message
  [log]
  (->> log
       (received-messages)
       (filter #(= "gameEnd" (:msgType %)))
       (first)))

(defn find-track
  [log]
  (get-in (find-game-init-message log)
          [:data :race :track]))

(defn car-position-messages
  [log]
  (->> log
       (received-messages)
       (filter #(= "carPositions" (:msgType %)))))

(defn car-specific-positions
  [car-position-messages]
  (mapcat (fn [{:keys [data gameTick]}]
            (map #(assoc % :game-tick gameTick) data)) car-position-messages))

(defn messages-between-ticks
  [log start-tick end-tick]
  (filter
   #(when-let [game-tick (:gameTick %)]
      (<= start-tick game-tick end-tick))
   (received-messages log)))


