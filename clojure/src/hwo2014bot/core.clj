(ns hwo2014bot.core
  (:require [hwo2014bot.system :as system])
  (:gen-class))

(defn -main[& [host port botname botkey]]
  (-> (system/system {:connection {:host host
                                   :port (Integer/parseInt port)
                                   :botname botname
                                   :botkey botkey}
                      :on-complete (fn [] (System/exit 0))})
      (system/start)))
