(ns hwo2014bot.bots.constant
  (:require [hwo2014bot.model.track :as track]
            [hwo2014bot.model.route :as route]
            [hwo2014bot.model.trip :as trip]))

(defmulti handle-msg :msgType)

(defmethod handle-msg "yourCar" [msg state]
  {:response {:msgType "ping" :data "ping"}
   :state (assoc state :car-color (get-in msg [:data :color]))})

(defmethod handle-msg "gameStart" [msg state]
  {:response {:msgType "ping" :data "ping"}
   :state (if-let [game-tick (:gameTick msg)]
            (assoc state :game-tick game-tick)
            state)})

(defmethod handle-msg "gameInit" [msg state]
  {:response {:msgType "ping" :data "ping"}
   :state (let [track (get-in msg [:data :race :track])
                entries (track/route-segment-lane-length-entries (track/route-segments track) (:lanes track))
                laps (get-in msg [:data :race :raceSession :laps])]
            (assoc state
              :track track
              :race-session-laps laps
              :track-segment-entries entries))})

(defmethod handle-msg "crash" [msg state]
  {:response {:msgType "ping" :data "ping"}
   :state (-> state
              (assoc :game-tick (:gameTick msg))
              (assoc :max-speed (:speed state)))})

(defmethod handle-msg "carPositions" [msg state]
  (let [game-tick (:gameTick msg)
        {:keys [track route]} state
        piece-count (count (:pieces track))
        our-car (first (filter #(= (:car-color state) (get-in % [:id :color])) (:data msg)))
        {piece-index :pieceIndex
         {start-lane-index :startLaneIndex end-lane-index :endLaneIndex} :lane
         lap :lap
         :as piece-position} (:piecePosition our-car)
        new-state (trip/update state piece-position)
        route (or (seq route) (:route (route/shortest-route (:track-segment-entries state) start-lane-index lap 2)))
        remaining-route (if (not (nil? piece-index))
                          (let [result (route/remaining-route route lap piece-index)]
                            (if-not (next result)
                              (route/continue-route result (:track-segment-entries state) 2)
                              result)))
        target-lane-index (route/target-lane-index remaining-route)
        throttle (if-let [max-speed (:max-speed state)]
                   (if (< 1.4 (Math/abs (- max-speed (:speed new-state))))
                     0.9
                     0.45)
                   1.0)
        
        response (if (and (not= target-lane-index (:target-lane-index state))
                          (not= end-lane-index target-lane-index))
                   {:msgType "switchLane" :data (if (< end-lane-index target-lane-index)
                                                  "Right" "Left")}
                   {:msgType "throttle" :data throttle})]
    {:response response
     :state (assoc new-state
              :route remaining-route
              :target-lane-index target-lane-index
              :throttle throttle
              :angle (:angle our-car)
              :game-tick game-tick
              :piece-position piece-position
              :throttle throttle)}))

(defmethod handle-msg "lapFinished" [msg state]
  (println msg)
  {:response {:msgType "ping" :data "ping"}
   :state state})

(defmethod handle-msg "tournamentEnd" [msg state]
  {:response nil
   :state state})

(defmethod handle-msg :default [msg state]
  {:response {:msgType "ping" :data "ping"}
   :state (if-let [game-tick (:gameTick msg)]
            (assoc state :game-tick game-tick)
            state)})

(defn create-bot
  []
  {:initial-state {:throttle 1.0
                   :trip 0.0
                   :speed 0.0
                   :max-speed 7.0
                   :acceleration 0.0
                   :piece-position nil
                   :max-bend-acceleration (track/bend-acceleration 6.5 100)}
   :handle-msg handle-msg})
