(ns hwo2014bot.channel
  (:require [clojure.data.json :as json]
            [aleph.tcp :refer [tcp-client]]
            [lamina.core :refer [enqueue wait-for-result wait-for-message]]
            [gloss.core :refer [string]]))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (when-let [message (try
                       (wait-for-message channel)
                       (catch Exception e
                         (println (str "ERROR: " (class e) (.getMessage e)))
                         nil))]
    (json->clj message)))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))
