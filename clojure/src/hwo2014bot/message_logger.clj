(ns hwo2014bot.message-logger)

(defn logging-handle-msg
  [log-atom handle-msg msg state]
  (let [result (handle-msg msg state)
        new-state (:state result)]
    (swap! log-atom conj
           {:type :received-message :message msg}
           {:type :sent-message :message (:response result)}
           {:type :state :data {:old (dissoc state :track :route :track-segment-entries)
                                :new (dissoc new-state :track :route :track-segment-entries)}})
    result))

(defn system
  [handle-msg]
  (let [log-atom (atom [])]
    {:log log-atom
     :handle-msg (partial logging-handle-msg log-atom handle-msg)}))

(defn start
  [system]
  system)

(defn stop
  [system]
  system)
