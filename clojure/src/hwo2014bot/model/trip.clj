(ns hwo2014bot.model.trip
  (:require [hwo2014bot.model.track :as track]))

(defn in-switching-lane? [piece-position]
  (not= (get-in piece-position [:lane :startLaneIndex])
        (get-in piece-position [:lane :endLaneIndex])))

(defn end-piece-distance [track piece-position]
  (let [in-piece-distance (:inPieceDistance piece-position)
        piece-index (:pieceIndex piece-position)
        piece (get-in track [:pieces piece-index])]
    (if (in-switching-lane? piece-position)
      (- (track/switch-length piece
                              (get-in piece-position [:lane :startLaneIndex])
                              (get-in piece-position [:lane :endLaneIndex]))
         in-piece-distance)
      (- (track/piece-length piece (get-in track [:lanes (get-in piece-position [:lane :startLaneIndex]) :distanceFromCenter]))
         in-piece-distance))))

(defn distance-between-piece-positions
  [track previous-piece-position piece-position]
  (if (= (:pieceIndex piece-position)
         (:pieceIndex previous-piece-position))
    (- (:inPieceDistance piece-position) (:inPieceDistance previous-piece-position))
    (+ (:inPieceDistance piece-position) (end-piece-distance track previous-piece-position))))

(defn update-trip
  [state piece-position]
  (let [{:keys [track trip] previous-piece-position :piece-position} state]
    (+ trip
       (if previous-piece-position
         (distance-between-piece-positions track previous-piece-position piece-position)
         0))))

(defn update
  [state piece-position]
  (let [new-trip (update-trip state piece-position)
        new-speed (- new-trip (:trip state))
        new-acceleration (- new-speed (:speed state))]
    (assoc state
      :piece-position piece-position
      :trip new-trip
      :speed new-speed
      :acceleration new-acceleration)))
