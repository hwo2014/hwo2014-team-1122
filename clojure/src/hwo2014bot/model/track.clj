(ns hwo2014bot.model.track
  (:require [hwo2014bot.math :as math]))

(defn bend?
  [piece]
  (contains? piece :angle))

(defn straight?
  [piece]
  (contains? piece :length))

(defn switch?
  [piece]
  (contains? piece :switch))

(defn straight-length
  [piece]
  (assert (straight? piece))
  (:length piece))

(defn straight-switch-length
  [piece start-lane-offset end-lane-offset]
  {:pre [(straight? piece) (switch? piece)]}
  (let [length (:length piece)]
    (let [lane-distance (- start-lane-offset end-lane-offset)]
      (Math/sqrt (+ (* length length) (* lane-distance lane-distance))))))

(defn bend-radius
  [piece distance-from-center]
  {:pre [(bend? piece)]}
  (let [{:keys [radius angle]} piece]
    (if (pos? angle)
      (- radius distance-from-center)
      (+ radius distance-from-center))))

(defn bend-length
  [piece distance-from-center]
  (assert (bend? piece))
  (let [{:keys [angle]} piece]
    (math/circular-arc-length (Math/abs angle) (bend-radius piece distance-from-center))))

(defn bend-switch-length
  [piece start-lane-offset end-lane-offset]
  (let [{:keys [radius angle]} piece]
    (let [abs-angle (Math/abs angle)
          target-lane-radius (bend-radius piece end-lane-offset)
          angle-in-radians (math/degrees->radians abs-angle)
          end-x (* target-lane-radius (Math/sin angle-in-radians))
          end-y (- target-lane-radius (* target-lane-radius (Math/cos angle-in-radians)))]
      (math/distance 0 start-lane-offset end-x end-y))))

(defn piece-length
  [piece distance-from-center]
  (cond
   (straight? piece) (straight-length piece)
   (bend? piece) (bend-length piece distance-from-center)))

(defn switch-length
  [piece start-lane-offset end-lane-offset]
  {:pre [(switch? piece)]}
  (cond
   (straight? piece) (straight-switch-length piece start-lane-offset end-lane-offset)
   (bend? piece) (bend-switch-length piece start-lane-offset end-lane-offset)))

(defn lane-distance-from-center
  [lanes lane-index]
  (get-in lanes [lane-index :distanceFromCenter]))

;; There's a maximum bend acceleration we can tolerate. We can find it out by crashing in the first bend. It should be
;; the same on each bend assuming constant "friction".

(defn bend-acceleration
  [speed radius]
  (/ (* speed speed) radius))

(defn speed-for-bend-acceleration
  [bend-acceleration radius]
  (Math/sqrt (* bend-acceleration radius)))

;; Is it possible to choose which route is better without knowing crashing bend acceleration?

(defn max-speed-for-piece
  [piece distance-from-center max-bend-acceleration]
  (cond
   (straight? piece) Double/MAX_VALUE
   (bend? piece) (speed-for-bend-acceleration
                  max-bend-acceleration
                  (bend-radius piece distance-from-center))))

(defn indexed-pieces
  [track]
  (map-indexed (fn [index piece] {:index index :piece piece}) track))

(defn route-segments
  [track]
  (->> (:pieces track)
       (indexed-pieces)
       (partition-by (comp :switch :piece))))

(defn direct-lanes
  [lanes]
  (map (fn [lane] {:start-lane lane :end-lane lane}) lanes))

(defn switching-lanes
  [lanes]
  (->> lanes
       (partition 2 1)
       (mapcat (fn [[a b]] [{:start-lane a :end-lane b}
                           {:start-lane b :end-lane a}]))))

(defn lane-combinations
  [track]
  (let [{:keys [lanes]} track]
    (concat (direct-lanes lanes) (switching-lanes lanes))))

(defn switch-segment?
  [route-segment]
  (and (not (next route-segment)) (switch? (:piece (first route-segment)))) )

(defn direct-lane-lenghts
  [route-segment lanes]
  (->> (direct-lanes lanes)
       (map
        (fn [{{distance-from-center :distanceFromCenter} :start-lane :as direct-lane}]
          [direct-lane (apply + (map #(piece-length (:piece %) distance-from-center) route-segment))]))
       (into {})))

(defn switch-lane-lenghts
  [route-segment lanes]
  {:pre [(switch-segment? route-segment)]}
  (let [[{:keys [piece]}] route-segment]
    (into
     (direct-lane-lenghts route-segment lanes)
     (map (fn [{{start-distance-from-center :distanceFromCenter} :start-lane
               {end-distance-from-center :distanceFromCenter}:end-lane
               :as switching-lane}]
            [switching-lane (switch-length piece start-distance-from-center end-distance-from-center)])
          (switching-lanes lanes)))))

(defn route-segment-lane-lengths
  [route-segment lanes]
  (cond
   (switch-segment? route-segment) (switch-lane-lenghts route-segment lanes)
   :else (direct-lane-lenghts route-segment lanes)))

(defn route-segment-lane-length-entries
  [route-segments lanes]
  (mapv
   (fn [route-segment]
     {:route-segment route-segment
      :lane-lengths (route-segment-lane-lengths route-segment lanes)})
   route-segments))
