(ns hwo2014bot.model.route
  (:require [hwo2014bot.model.track]))

(defn- add-lane-route
  [route route-segment lap lane-choice]
  (conj route {:route-segment route-segment
               :lap lap
               :lane-choice lane-choice}))

(defn- create-initial-routes
  [first-route-entry]
  (let [{{:keys [route-segment lane-lengths] :as route-entry} :route-entry lap :lap} first-route-entry]
    (map
     (fn [[lane-choice length]]
       {:total-length length
        :route (add-lane-route [] route-segment lap lane-choice)})
     lane-lengths)))

(defn- add-matching-lane-choices
  [results {{:keys [route-segment lane-lengths]} :route-entry lap :lap}]
  (for [{:keys [total-length route] :as result-entry} results
        [current-lane-choice length] lane-lengths
        :let [{previous-lane-choice :lane-choice} (peek route)]
        :when (= (:end-lane previous-lane-choice) (:start-lane current-lane-choice))]
    {:total-length (+ total-length length)
     :route (add-lane-route route route-segment lap current-lane-choice) }))

(defn- choose-shortest-route-for-each-lane
  [results]
  (->> results
       (group-by #(:end-lane (:lane-choice (peek (:route %)))))
       (map (fn [[end-lane result-entries]]
              (first (sort-by :total-length result-entries))))))

(defn- lap-route-entries
  [entries start-lap laps]
  (mapcat (fn [lap] (map (fn [route-entry] {:lap lap :route-entry route-entry}) entries)) (range start-lap (+ start-lap laps))))

(defn- choose-shortest-route
  [results]
  (reduce (fn [shortest-route route]
            (if (< (:total-length route) (:total-length shortest-route)) route shortest-route))
          results))

(defn shortest-route
  [entries start-lane-index start-lap laps]
  (let [[first-route-entry & rest-of-route-entries] (lap-route-entries entries start-lap laps)]
    (choose-shortest-route
     (reduce
      (fn [results lap-route-entry]
        (-> results
            (add-matching-lane-choices lap-route-entry)
            (choose-shortest-route-for-each-lane)))
      (filter
       (fn [{:keys [route]}]
         (= start-lane-index (:index (:start-lane (:lane-choice (first route))))))
       (create-initial-routes first-route-entry))
      rest-of-route-entries))))

(defn remaining-route
  [route lap piece-index]
  (drop-while
   (fn [lap-route-entry]
     (let [route-entry-piece-index (:index (first (:route-segment lap-route-entry)))]
       (or (and (= (:lap lap-route-entry) lap) (< route-entry-piece-index piece-index))
           (< (:lap lap-route-entry) lap))))
   route))

(defn continue-route
  [route entries laps]
  (let [{{{end-lane-index :index} :end-lane} :lane-choice lap :lap} (last route)]
    (concat route (:route (shortest-route entries end-lane-index (inc lap) 3)))))

(defn target-lane-index
  [route]
  (get-in (first route) [:lane-choice :end-lane :index]))
