(ns hwo2014bot.math)

(def degree-in-radians (/ Math/PI 180.0))

(defn degrees->radians
  [degrees]
  (* degrees degree-in-radians))

(defn circular-arc-length
  [angle-in-degrees radius]
  (* (degrees->radians angle-in-degrees) radius))

(defn average
  [& vals]
  (loop [vals vals sum 0 cnt 0]
    (if (seq vals)
      (recur (rest vals) (+ sum (first vals)) (inc cnt))
      (/ sum cnt))))

(defn distance
  [x1 y1 x2 y2]
  (let [dx (- x1 x2) dy (- y1 y2)]
    (Math/sqrt (+ (* dx dx) (* dy dy)))))
