(ns hwo2014bot.system
  (:require [hwo2014bot.channel :refer [connect-client-channel send-message]]
            [hwo2014bot.bots.constant :as constant-bot]
            [hwo2014bot.channel :refer [read-message send-message]]
            [hwo2014bot.message-logger :as message-logger]
            [hwo2014bot.race-files :as race-files]))

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [bot channel running]
  (let [handle-msg (:handle-msg bot)]
    (loop [initial-state (:initial-state bot)]
      (if (not @running)
        (println "Stopped game loop")
        (when-let [msg (read-message channel)]
          (log-msg msg)
          (let [new-state (try
                            (handle-msg msg initial-state)
                            (catch Exception e
                              (.printStackTrace e)
                              (throw e)))]
            (when-let [response (:response new-state)]
              (send-message channel response)
              (recur (:state new-state)))))))))

(defn get-message-log
  [system]
  @(:log (:message-logger system)))

(defn save-race
  [system]
  (let [log (get-message-log system)]
    (race-files/save-race log)))

(defn load-race
  [f]
  (race-files/load-race f))

(defn run-on-complete
  [system]
  (when-let [on-complete (:on-complete system)]
    (on-complete)))

(defn system
  ([]
     (system {}))
  ([{{:keys [host port botname botkey]
      :or {host "hakkinen.helloworldopen.com" port 8091 botname "Crasher" botkey "gPMjMShkoQYPxw"}} :connection
      on-complete :on-complete
      handle-msg-wrapper :handle-msg-wrapper}]
     (let [bot (constant-bot/create-bot)
           message-logger (message-logger/system (cond-> (:handle-msg bot)
                                                   handle-msg-wrapper handle-msg-wrapper))]
       {:host host
        :port port
        :botname botname
        :botkey botkey
        :message-logger message-logger
        :bot (assoc bot :handle-msg (:handle-msg message-logger))
        :on-complete on-complete})))

(defn start
  [system]
  (let [{:keys [host port botname botkey bot]} system
        channel (let [channel (connect-client-channel host port)]
                  (send-message channel {:msgType "join" :data {:name botname :key botkey}})
                  channel)
        running (atom true)]
    (-> system
        (update-in [:message-logger] message-logger/start)
        (assoc :running running
               :start-date (java.util.Date.)
               :channel channel
               :game-loop (future (let [result (game-loop bot channel running)]
                                    (run-on-complete system)
                                    result))))))

(defn stop
  [system]
  (-> system
      (update-in [:message-logger] message-logger/stop)
      (update-in [:running] (fn [x] (reset! x false) x))))

