(ns hwo2014bot.race-files
  (:require [clojure.java.shell :refer [sh]]
            [hwo2014bot.message-log :as message-log]
            [clojure.java.io :as io]
            [clojure.edn :as edn]))

(defn git-revision
  []
  (let [{:keys [exit out]} (sh "git" "rev-parse" "HEAD")]
    (when (zero? exit)
      (.substring out 0 (dec (.length out))))))

(defn save-race
  [log]
  (let [game-id (message-log/find-game-id log)]
    (spit (str "races/" game-id ".edn") {:git-revision (git-revision)
                                         :log log})))

(defn list-files
  []
  (sort-by #(.lastModified %) (filter #(and (.isFile %)) (file-seq (io/file "races")))))

(defn load-race
  [f]
  (with-open [reader (java.io.PushbackReader. (io/reader f))]
    (edn/read reader)))

(defn load-latest-race
  []
  (when-let [f (last (list-files))]
    (load-race f)))


